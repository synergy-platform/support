<?php

namespace Synergy\Support\Traits;

/**
 * Trait that adds common methods to repositories.
 *
 * Part of the Support package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Support
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/support
 */

trait Repository {

	/**
	 * Create a new instance of the model.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function createModel(array $data = [])
	{
		$class = '\\'.ltrim($this->model, '\\');

		return new $class($data);
	}

	/**
	 * Returns the model.
	 *
	 * @return string
	 */
	public function getModel()
	{
		return $this->model;
	}

	/**
	 * Set the name of the model
	 *
	 * @param  string  $model
	 * @return $this
	 */
	public function setModel($model)
	{
		$this->model = $model;

		return $this;
	}

	/**
	 * Dynamically pass missing methods to the model.
	 *
	 * @param  string  $method
	 * @param  array  $parameters
	 * @return mixed
	 */
	public function __call($method, $parameters)
	{
		$model = $this->createModel();

		return call_user_func_array([$model, $method], $parameters);
	}
}