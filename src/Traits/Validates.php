<?php

namespace ShaneDaniels\Support\Traits;

use ShaneDaniels\Support\Validator;

/**
 * Part of the Support package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Support
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/support
 */

trait Validates
{
	/**
	 * The Validator instance.
	 *
	 * @var \ShaneDaniels\Support\Validator
	 */
	protected $validator;

	/**
	 * Returns the Validator instance.
	 *
	 * @return \ShaneDaniels\Support\Validator
	 */
	public function getValidator()
	{
		return $this->validator;
	}

	/**
	 * Sets the Validator instance.
	 *
	 * @param  \ShaneDaniels\Support\Validator  $validator
	 * @return $this
	 */
	public function setValidator(Validator $validator)
	{
		$this->validator = $validator;
		return $this;
	}
}