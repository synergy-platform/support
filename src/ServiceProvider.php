<?php

namespace Synergy\Support;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

/**
 * Extends the default Illuminate Service Provider
 *
 * Part of the Support package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Support
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/support
 */

abstract class ServiceProvider extends IlluminateServiceProvider
{
	/**
	 * Registers middleware with the routes instance
	 *
	 * @param array $middlewares
	 */
	protected function middleware(array $middlewares = [])
	{
		foreach ($middlewares as $key => $class) {
			$this->app['router']->middleware($key, $class);
		}
	}


	/**
	 * Registers event subscribers with the Illuminate
	 * eventing system
	 *
	 * @param array $subscribers
	 */
	protected function subscribe(array $subscribers = [])
	{
		foreach ($subscribers as $subscriber) {
			$this->app['events']->subscribe($subscriber);
		}
	}
}