<?php

/**
 * File containing helper methods.
 *
 * Part of the Support package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Support
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/Support
 */

if ( ! function_exists('group')) {
	/**
	 * Create a route group with shared attributes.
	 *
	 * @param  array     $attributes
	 * @param  \Closure  $callback
	 * @return \Illuminate\Routing\Route
	 */
	function route_group(array $attributes, Closure $callback)
	{
		return app('router')->group($attributes, $callback);
	}
}

if ( ! function_exists('input'))
{
	/**
	 * Returns an instance of the input.
	 *
	 * @param  string|null  $key
	 * @param  string|null  $default
	 * @return mixed
	 */
	function input($key = null, $default = null)
	{
		if ( ! is_null($key)) {
			return app('request')->input($key, $default);
		}

		return app('request');
	}
}

if ( ! function_exists('request')) {

	/**
	 * Returns an instance of the http request.
	 *
	 * @return \Illuminate\Http\Request
	 */
	function request()
	{
		return app('request');
	}
}

if ( ! function_exists('vendor_path')) {

	/**
	 * Get the path to the vendor folder.
	 *
	 * @param  string  $path
	 * @return string
	 */
	function vendor_path($path = '')
	{
		return base_path('vendor').($path ? DIRECTORY_SEPARATOR.$path : $path);
	}
}